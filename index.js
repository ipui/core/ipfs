import React, { Component, createContext } from 'react'
import PropTypes from 'prop-types'
import IpfsHttpClient from 'ipfs-http-client'

import RouteTools from '../lib/RouteTools'

const IpfsContext = createContext()

class Ipfs extends Component {

  state = {
    address: null,
    node: null
  }

  static propTypes = {
    noIpfs: PropTypes.elementType
  }

  constructor( props ) {
    super( props )
    this.connect = this.connect.bind( this )
    this.bootstrap = this.bootstrap.bind( this )
  }

  static getDerivedStateFromError( error ) {
    return { hasError: true }
  }

  componentDidMount() {
    this.bootstrap()
  }

  bootstrap() {
    const parsed = RouteTools.parse()

    if ( !parsed )
      RouteTools.redirect( '/' )

    if ( !parsed.addresses ) return

    this.connect( parsed.addresses.api )
  }

  connect( address ) {
    const node = IpfsHttpClient( address )
    node.id()
      .then( identity => {
        this.setState( ( prevState, props ) => {
          return {
            ...prevState,
            address,
            identity,
            node
          }
        } )
      } )
      .catch( console.error )
  }

  render() {
    const { node, address, identity } = this.state

    if ( !node ) {
      const { noIpfs, links } = this.props
      if ( noIpfs )
        return React.createElement( noIpfs, { links } )

      return ( <>an <a href="https://ipfs.io">ipfs</a> node is required to use this dapp</> )
    }

    return (
      <IpfsContext.Provider value={ {
        address,
        node,
        identity
      } }>
        { this.props.children }
      </IpfsContext.Provider>
    )
  }

}

const withIpfs = ( ComponentAlias ) => {

  return props => (
    <IpfsContext.Consumer>
      { context => {
        return <ComponentAlias { ...props } withIpfs={ context } />
      } }
    </IpfsContext.Consumer>
  )

}

export default Ipfs

export {
  IpfsContext,
  withIpfs
}
